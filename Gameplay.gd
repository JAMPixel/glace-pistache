extends Node2D

const BobPart = preload("res://BobPart.tscn")

var waitForNewBob = 0

#func _ready():
	#var bob1 = BobPart.instance()
	#$Bobs.add_child(bob1)	
	
func _input(event):
	if event.is_action_pressed("click"):
		toggleTimerToDisplayBobs()
			
func toggleTimerToDisplayBobs():
	if $Timer.is_stopped() == false:
		$Timer.stop()
	else:
		$Timer.start(1)
		
func forceTimerToStop():
	if $Timer.is_stopped() == false:
		$Timer.stop()
	
func isThereABobHereAlready(wantedX, wantedY):
	var totBobs = $Bobs.get_child_count()
	if $Bobs.get_child_count() == 0:
		return false
	var noBobsLand = 50
	var i = 0
	for currentBob in $Bobs.get_children():
		i = i + 1
		if currentBob == null:
			return false
		if (wantedX > currentBob.position.x - noBobsLand && 
			wantedX < currentBob.position.x + noBobsLand &&
			wantedY > currentBob.position.y - noBobsLand && 
			wantedY < currentBob.position.y + noBobsLand):
			return true
	return false
	
func createRandomBobinou():
	var viewportWidth = int(get_viewport_rect().size.x)
	var viewportHeight = int(get_viewport_rect().size.y)
	var randomX = randi() % viewportWidth + 1
	var randomY = randi() % viewportHeight + 1
	var tries = 1
	while isThereABobHereAlready(randomX, randomY) == true:
		$Text_tries.text = String(tries)
		tries = tries + 1
		if tries >= 8000:
			forceTimerToStop()
			return
		randomX = randi() % viewportWidth + 1
		randomY = randi() % viewportHeight + 1
	createBobinou(randomX, randomY)
		
func createBobinou(x, y):
	var bobinou = BobPart.instance()
	bobinou.position.x += x
	bobinou.position.y += y
	$Bobs.add_child(bobinou)

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
		
	# display bobs if Timer is running, each 50 frame
	waitForNewBob = waitForNewBob + 1
	if $Timer.is_stopped() == false && waitForNewBob >= 20:		
		waitForNewBob = 0
		createRandomBobinou()
