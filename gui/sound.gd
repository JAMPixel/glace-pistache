extends AudioStreamPlayer

var res_sound = "res://assets/glacepistache_cloche_"
var sound = [res_sound +"Do.wav", res_sound +"Fa.wav",
res_sound +"Si.wav", res_sound +"La.wav"]
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func play_sound():
	stream = load(sound[randi()%4])
	play(0)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
