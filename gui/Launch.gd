extends TextureButton

signal play_sound
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func launch_game():
	get_tree().change_scene("res://gameplay.tscn")
	emit_signal("play_sound")

func _on_Launch_button_up():
	launch_game()


