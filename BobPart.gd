extends Node2D

var partAbove: Node2D
var partBelow: Node2D

export(Vector2) var velocity = Vector2.ZERO
export(float) var rotSpeed = 0.0

func _ready():
	pass

func _process(delta):
	var rotVec = Vector2(cos(rotation), sin(rotation))
	var posOffs = Vector2.ZERO
	
	if partAbove != null:
		var attachmentOffset = partAbove.get_node("AttachmentBelow").get_global_transform().get_origin() - $AttachmentAbove.get_global_transform().get_origin()
		posOffs += attachmentOffset
		rotVec += 0.1 * Vector2(cos(partAbove.rotation), sin(partAbove.rotation))
	
	if partBelow != null:
		var attachmentOffset = partBelow.get_node("AttachmentAbove").get_global_transform().get_origin() - $AttachmentBelow.get_global_transform().get_origin()
		posOffs += attachmentOffset
		rotVec += 0.1 * Vector2(cos(partBelow.rotation), sin(partBelow.rotation))
	
	position += 0.01 * posOffs
	rotation = atan2(rotVec.y, rotVec.x)
	
	position += delta * velocity
	rotation += delta * rotSpeed

func _enter_tree():
	if partBelow != null:
		partBelow.position.y += 32

func attachAbove(otherBob: Node2D):
	partBelow = otherBob
	otherBob.partAbove = self

func attachBelow(otherBob: Node2D):
	partAbove = otherBob
	otherBob.partBelow = self
	
func _on_AttachmentAbove_area_entered(area: Area2D):
	if partAbove != null:
		return
	var otherBob = area.get_parent()
	attachBelow(otherBob)
	
func _on_AttachmentBelow_area_entered(area: Area2D):
	if partBelow != null:
		return
	var otherBob = area.get_parent()
	attachAbove(otherBob)
