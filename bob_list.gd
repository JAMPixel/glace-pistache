extends Node2D

var wanted_size
var bob_texture = []
const bob = preload("res://gui/SnowMan.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	add_bob_on_the_list()
	add_bob_on_the_list()
	add_bob_on_the_list()
	pass # Replace with function body.

func add_bob_on_the_list():
	var texture = TextureRect.new();
	var bob_instance = bob.instance()
	add_child(bob_instance)
	bob_instance.load_snow_men(randi()%3, randi()%3, randi()%3)
	var projectResolution=get_viewport_rect().size
	wanted_size = projectResolution.y*0.2
	texture.texture = load("res://images/timer_placeholder.png")
#	bob_instance.scale = compute_size(texture.texture.get_size())
	texture.rect_scale = compute_size(texture.texture.get_size())
#	add_child(texture)
	bob_texture.push_front(texture)
	var i = 0
	for btexture in bob_texture:
		btexture.rect_position = compute_position(projectResolution, i)
		bob_instance.position = compute_position(projectResolution, i)
		i += 1
	
func compute_position(screen_resolution, number_of_bob):
	return Vector2(number_of_bob*wanted_size, screen_resolution.y*0.8)
	
func compute_size(size):
	var scale_x =  wanted_size / size.x
	var scale_y = wanted_size / size.y 
	return Vector2(scale_x, scale_y)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_released("ui_accept"):
		add_bob_on_the_list()
	pass
