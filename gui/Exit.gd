extends TextureButton
signal play_sound
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func exit_game():
	emit_signal("play_sound")
	get_node("../Animation").play("fondu")
#	

func _on_Exit_button_up():
	exit_game()


func _on_Animation_animation_finished(anim_name):
	get_tree().quit()
