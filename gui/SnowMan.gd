extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func load_snow_men(head, body, feet):
	get_node("head").change_texture(head)
	get_node("body").change_texture(body)
	get_node("feet").change_texture(feet)
	pass
